# Machine Learning with Python courses by ibm

After I got my certificate in machine learning course of  Andrew Ng course I wanted to work on some project but
with the python language not Octave. So I took this course.

As the level of this course was for beginner in ML, I went beyond what was taugth in this course.

So here I have been able to work with:
- Linear, Non-linear, Simple and Multiple regression
- Classification algorithms, such as KNN, Decision Trees, Logistic Regression and SVM
- Different classification accuracy metrics
- 3 main types of clustering, including Partitioned-based Clustering, Hierarchical Clustering, and Density-based Clustering 
- 2 main types of recommendation engines, namely, content-based and collaborative filtering

For each algoritmh I worked with, I added a new part related to predicion result on new or unacquired data.
Becauce prediction it's why did what we have done.

You can therefore have an overview with:
- Hierarchical Clustering where I used a knn method to predict value for a new car based on our Hierarchical Clustering results.
- collaborative filtering where I use a model-based using a SVD (singular value decomposition) with scikit surprise lib to predict values.
- and so on...
